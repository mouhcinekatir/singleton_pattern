package singleton_Pattern;

public class Singleton {

	private Singleton() {
		super();
	}
	private static class SingletonHolder {
		private final static Singleton singleton = new Singleton();
	}
	public static Singleton getInstance(){
		return SingletonHolder.singleton;
	}
	public void traitement() {
		System.out.println("traitement de singleton");
	}
}
