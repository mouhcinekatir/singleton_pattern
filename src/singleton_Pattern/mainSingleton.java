package singleton_Pattern;

public class mainSingleton {

	public static void main(String[] args) {
		Singleton sing = Singleton.getInstance();
		
		sing.traitement();
	}

}
